# Dataset

## local

These are recorded as csv files from the unity VR environment.

## remote

There are recorded as rosbags with the following relevant fields:

### VPN, no img

**File:** "2021-05-26-17-49-23.bag"

|                  Topic                   |          Msg Type          | Msgs Type | Frequency [Hz] |
| :--------------------------------------: | :------------------------: | :-------: | :------------: |
| /joint_group_position_controller/command | std_msgs/Float64MultiArray |  428601   |  6034.969784   |
|              /joint_states               |   sensor_msgs/JointState   |   8959    |   137.290846   |
|        /relaxed_ik/ee_pose_goals         |   relaxed_ik/EEPoseGoals   |   7167    |   100.007248   |
|                   /tf                    |     tf2_msgs/TFMessage     |  442102   |  6150.005865   |
|                /timestamp                |      std_msgs/Float64      |    72     |    1.000002    |
|             /unity/gripper1              |      std_msgs/Float32      |   3104    |   120.741090   |
|              /unity/twist1               |    geometry_msgs/Twist     |   3103    |   120.624765   |

### VPN, two cameras

**File:** "2021-05-26-17-53-00.bag"
|                  Topic                   |          Msg Type          | Msgs Type | Frequency [Hz] |
| :--------------------------------------: | :------------------------: | :-------: | :------------: |
| /joint_group_position_controller/command | std_msgs/Float64MultiArray |  199057   |  5866.159441   |
|              /joint_states               |   sensor_msgs/JointState   |   4297    |   137.304329   |
|        /relaxed_ik/ee_pose_goals         |   relaxed_ik/EEPoseGoals   |   3438    |   100.009633   |
|                   /tf                    |     tf2_msgs/TFMessage     |  205674   |  5991.862857   |
|                /timestamp                |      std_msgs/Float64      |    35     |    0.999998    |
|             /unity/gripper1              |      std_msgs/Float32      |   3750    |   123.369139   |
|              /unity/twist1               |    geometry_msgs/Twist     |   3749    |   123.068689   |

### Ethernet, no img

**File:** "2021-05-26-18-07-03.bag"
|                  Topic                   |          Msg Type          | Msgs Type | Frequency [Hz] |
| :--------------------------------------: | :------------------------: | :-------: | :------------: |
| /joint_group_position_controller/command | std_msgs/Float64MultiArray |  211323   |  6009.031519   |
|              /joint_states               |   sensor_msgs/JointState   |   4443    |   137.356039   |
|        /relaxed_ik/ee_pose_goals         |   relaxed_ik/EEPoseGoals   |   3555    |   100.002480   |
|                   /tf                    |     tf2_msgs/TFMessage     |  218042   |  6132.023392   |
|             /unity/gripper1              |      std_msgs/Float32      |   3878    |   124.404686   |
|              /unity/twist1               |    geometry_msgs/Twist     |   3878    |   119.868080   |

### Ethernet, two cameras

**File:** "2021-05-26-18-08-27.bag"
|                  Topic                   |          Msg Type          | Msgs Type | Frequency [Hz] |
| :--------------------------------------: | :------------------------: | :-------: | :------------: |
| /joint_group_position_controller/command | std_msgs/Float64MultiArray |  291433   |  5882.614306   |
|              /joint_states               |   sensor_msgs/JointState   |   6274    |   137.369535   |
|        /relaxed_ik/ee_pose_goals         |   relaxed_ik/EEPoseGoals   |   5020    |   100.000095   |
|                   /tf                    |     tf2_msgs/TFMessage     |  301062   |  6009.031519   |
|             /unity/gripper1              |      std_msgs/Float32      |   5467    |   122.443556   |
|              /unity/twist1               |    geometry_msgs/Twist     |   5467    |   115.999336   |
