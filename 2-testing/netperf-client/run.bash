#!/bin/bash

########################
# CLIENT
INSTANCE_NAME="netperf-client"
########################

docker run -it \
    --name=$INSTANCE_NAME \
    --rm --privileged \
    --net=host \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    $INSTANCE_NAME
