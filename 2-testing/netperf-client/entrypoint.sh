#!/bin/sh
########################
# CLIENT VERSION
# https://cloud.google.com/blog/products/networking/using-netperf-and-ping-to-measure-network-latency
# https://hewlettpackard.github.io/netperf/doc/netperf.html#TCP_005fSTREAM

# Set your testing IP address
export TEST_ADDRESS=192.168.42.10

# Initial ping test for open ICMP port
sudo ping $TEST_ADDRESS -c 10 -i 0.001

# Run TCP RR
netperf -H $TEST_ADDRESS -l 10 -t TCP_RR -w 10ms -b 1 -v 2 -- -O min_latency,mean_latency,max_latency,stddev_latency,transaction_rate

# Run UDP RR
netperf -H $TEST_ADDRESS -l 10 -t UDP_RR -w 10ms -b 1 -v 2 -- -O min_latency,mean_latency,max_latency,stddev_latency,transaction_rate

# Run TCP STREAM
netperf -H $TEST_ADDRESS -l 10 -t UDP_STREAM -f M -v 2

# Run UDP STREAM
netperf -H $TEST_ADDRESS -l 10 -t TCP_STREAM -f M -v 2
########################
