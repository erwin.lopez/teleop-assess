# Testing scripts

These scripts compile netperf with fine grained latency capabilities. Use the docker files or compile it natively using the instructions on the Dockerfile.

To use the client, modify the _TEST_ADDRESS_ variable in the **entrypoint.sh** file.
