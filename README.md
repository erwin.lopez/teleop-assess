# Assessing robotic remote teleoperation

Testing scripts and data for the paper "Implementing and assessing a remote teleoperation setup with a Digital Twin using cloud networking"

Each folder includes a script or data relevant to the work presented in this paper.

## 1-Configuration Scripts

All the script used to configure the AWS server are available in the **vpn** folder. This can be replicated in any cloud-provider.

A sample of the modifications done to the Unity scripts used in the local side are available in the **unity** folder.

## 2-Testing Scripts

The network testing scripts used to measure bandwidth and delays are in the **netperf** folder. You can run them using docker or running them natively in your Linux machines.

## 3-Analysis Scripts

The python scripts used to analyse the recorded rosbags and unity-generated csv are in this folder.

## Data

This is the data recorded during the experiments from both local and remote sides.

## Slides

Coming soon...

## Citation

If you think this work is useful, please consider citing it:

``` latex
@misc{erwin_lopez_2022_6840823,
  author       = {Erwin Lopez and
                  Hanlin Niu and
                  Guido Herrmann and
                  Joaquin Carrasco},
  title        = {{Code for: Implementing and assessing a remote teleoperation setup with a Digital Twin using cloud networking}},
  month        = jul,
  year         = 2022,
  publisher    = {Zenodo},
  doi          = {10.5281/zenodo.6840823},
  url          = {https://doi.org/10.5281/zenodo.6840823}
}
```
