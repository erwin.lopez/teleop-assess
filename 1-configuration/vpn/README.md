# Configurate AWS

## Docker configuration

The IPsec/L2TP setup was based on this version of the **hwdsl2** vpn-server. It runs on Debian, with libreswan 4.3.

<https://github.com/hwdsl2/docker-ipsec-vpn-server/blob/d6dece431f5013483dde4d0bbd7be60cbd1787d0/Dockerfile>

No DNS server was used.

Clients were configured, with a setup following these instructions:

<https://github.com/hwdsl2/setup-ipsec-vpn/blob/master/docs/clients.md>

### Build and Run

``` shell
git clone https://github.com/hwdsl2/docker-ipsec-vpn-server/tree/d6dece431f5013483dde4d0bbd7be60cbd1787d0
cd docker-ipsec-vpn-server
# To build the image
docker build -t hwdsl2/ipsec-vpn-server .
# To run the server
docker run \
    --name ipsec-vpn-server \
    --restart=always \
    -v vpn-data:/etc/ipsec.d \
    -p 500:500/udp \
    -p 4500:4500/udp \
    -d --privileged \
    hwdsl2/ipsec-vpn-server
```

## Open ports

Open UDP  ports **500** and **4500** in any network configuration panel you are using.

## Future use

It is recommended to use the latest version and test the best performing configuration in your system. For Linux devices, wireguard might perform better overall.
