﻿/*
© Siemens AG, 2017-2018
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)
Changelog:
25-05-2021-Logging messages to individual csv file
Dr. Erwin Lopez (erwin.lopezpulgarin@manchester.ac.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using System.Threading.Tasks;

namespace RosSharp.RosBridgeClient
{
    public class JointStateSubscriber : Subscriber<Messages.Sensor.JointState>
    {
        public List<string> JointNames;
        public List<JointStateWriter> JointStateWriters;
        public string filePath = "./";
        public string fileName = "_Joints.csv";
        public bool fileHeader = false;

        void Awake()
        {
            long time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            Debug.Log(time);
            fileName = time + fileName;
            Debug.Log(fileName);
            filePath = Application.persistentDataPath + "/" + fileName;
            Debug.Log(filePath);

            // This text is added only once to the file.
            if (!File.Exists(filePath))
            {
                // Create a file to write to.
                string createText = Environment.NewLine;
                File.WriteAllText(filePath, createText, Encoding.UTF8);
            }
        }
        public void LogJointsToCsv(Messages.Sensor.JointState message)
        {
            long time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            string text = "";
            if (!fileHeader)
            {
                text += "time,";
                for (int i = 0; i < message.name.Length; i++)
                {
                    text += message.name[i] + ",";
                }
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);

                text = time + ",";
                for (int i = 0; i < message.name.Length; i++)
                {
                    text += message.position[i] + ",";
                }
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);

                fileHeader = true;
            }
            else
            {
                text = time + ",";
                for (int i = 0; i < message.name.Length; i++)
                {
                    text += message.position[i] + ",";
                }
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);
            }
        }
        protected override void ReceiveMessage(Messages.Sensor.JointState message)
        {
            int index;

            for (int i = 0; i < message.name.Length; i++)
            {
                index = JointNames.IndexOf(message.name[i]);
                if (index != -1)
                    JointStateWriters[index].Write(message.position[i]);
            }
            LogJointsToCsv(message);
        }
    }
}

