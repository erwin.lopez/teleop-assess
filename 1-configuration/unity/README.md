# Unity files

These are ROSSharp and ROS Bridge modified callback functions. They add CSV logging capabilities to a Topic Callback function. Use them to modify your own Callback function.

Consider offloading the disk-writting functions to a [Coroutine](https://docs.unity3d.com/Manual/Coroutines.html) if your message rate is too high, to avoid hang-ups and unwanted behaviour whilst logging data.
