﻿/*
Author: Dr Inmo Jang (inmo.jang@manchester.ac.uk)
Changelog:
25-05-2021-Logging messages to individual csv file
Dr. Erwin Lopez (erwin.lopezpulgarin@manchester.ac.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Text;
using System;
using System.Threading.Tasks;
namespace RosSharp.RosBridgeClient
{
    public class GripperFloat32Publisher : Publisher<Messages.Standard.Float32>
    {
        public GameObject controlScripts;
        private GripperControl gripperControl_script;

        private Messages.Standard.Float32 message;
        public string filePath = "./";
        public string fileName = "_Gripper.csv";
        public bool fileHeader = false;
        protected override void Start()
        {
            base.Start();
            InitializeMessage();
            gripperControl_script = controlScripts.GetComponent<GripperControl>();

            long time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            Debug.Log(time);
            fileName = time + fileName;
            Debug.Log(fileName);
            filePath = Application.persistentDataPath + "/" + fileName;
            Debug.Log(filePath);

            // This text is added only once to the file.
            if (!File.Exists(filePath))
            {
                // Create a file to write to.
                string createText = Environment.NewLine;
                File.WriteAllText(filePath, createText, Encoding.UTF8);
            }
        }

        public void LogGripperToCsv(float grab_strength)
        {
            long time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            string text = "";
            if (!fileHeader)
            {
                text += "time,grab_strength";
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);

                text = time + ",";
                text += grab_strength + ",";
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);

                fileHeader = true;
            }
            else
            {
                text = time + ",";
                text += grab_strength + ",";
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);
            }
        }
        private void FixedUpdate()
        {
            UpdateMessage();
        }

        private void InitializeMessage()
        {
            message = new Messages.Standard.Float32();
            message.data = 0;
        }

        private void UpdateMessage()
        {

            float grab_strength = gripperControl_script.grab_strength;
            message.data = grab_strength;

            Publish(message);
            LogGripperToCsv(grab_strength);
        }
    }

}