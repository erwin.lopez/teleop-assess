﻿/*
© Siemens AG, 2017-2018
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)
Changelog:
25-05-2021-Logging messages to individual csv file
Dr. Erwin Lopez (erwin.lopezpulgarin@manchester.ac.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using Leap;
using Leap.Unity;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using System.Threading.Tasks;
namespace RosSharp.RosBridgeClient
{
    public class MarkerTwistPosePublisher : Publisher<Messages.Geometry.Twist>
    {
        public GameObject controlMarker;
        private MarkerModel controlMarker_script;

        private Messages.Geometry.Twist message;

        public string filePath = "./";
        public string fileName = "_Twist.csv";
        public bool fileHeader = false;
        protected override void Start()
        {
            base.Start();
            InitializeMessage();
            controlMarker_script = controlMarker.GetComponent<MarkerModel>();

            long time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            Debug.Log(time);
            fileName = time + fileName;
            Debug.Log(fileName);
            filePath = Application.persistentDataPath + "/" + fileName;
            Debug.Log(filePath);

            // This text is added only once to the file.
            if (!File.Exists(filePath))
            {
                // Create a file to write to.
                string createText = Environment.NewLine;
                File.WriteAllText(filePath, createText, Encoding.UTF8);
            }
        }
        public void LogTwistToCsv(Messages.Geometry.Vector3 linear, Messages.Geometry.Vector3 angular)
        {
            long time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            string text = "";
            if (!fileHeader)
            {
                text += "time,l.x,l.y,l.z,a.x,a.y,a.z";
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);

                text = time + ",";
                text += linear.x + ",";
                text += linear.y + ",";
                text += linear.z + ",";
                text += angular.x + ",";
                text += angular.y + ",";
                text += angular.z + ",";
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);

                fileHeader = true;
            }
            else
            {
                text = time + ",";
                text += linear.x + ",";
                text += linear.y + ",";
                text += linear.z + ",";
                text += angular.x + ",";
                text += angular.y + ",";
                text += angular.z + ",";
                text += "\n";
                File.AppendAllText(filePath, text, Encoding.UTF8);
            }
        }
        private void FixedUpdate()
        {
            UpdateMessage();
        }

        private void InitializeMessage()
        {
            message = new Messages.Geometry.Twist();
            message.linear = new Messages.Geometry.Vector3();
            message.angular = new Messages.Geometry.Vector3();
        }
        private void UpdateMessage()
        {
            // Position Control - By Grasping
            Vector3 linearDisplacement = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 InitialMarkerPosition = controlMarker_script.GetInitialPosition();
            linearDisplacement = controlMarker.transform.position - InitialMarkerPosition;
            message.linear = GetGeometryVector3(linearDisplacement.Unity2Ros());

            // Orientation Control - By Pinching
            Vector3 angularDisplacement = new Vector3(0.0f, 0.0f, 0.0f);
            Quaternion InitialMarkerRotation = controlMarker_script.GetInitialRotation();
            Quaternion quatDisaplcement = controlMarker.transform.rotation * Quaternion.Inverse(InitialMarkerRotation);
            angularDisplacement = quatDisaplcement.eulerAngles * Mathf.Deg2Rad;
            message.angular = GetGeometryVector3(-angularDisplacement.Unity2Ros());

            Publish(message);
            LogTwistToCsv(message.linear, message.angular);
        }

        private static Messages.Geometry.Vector3 GetGeometryVector3(Vector3 vector3)
        {
            Messages.Geometry.Vector3 geometryVector3 = new Messages.Geometry.Vector3();
            geometryVector3.x = vector3.x;
            geometryVector3.y = vector3.y;
            geometryVector3.z = vector3.z;
            return geometryVector3;
        }
    }
}
