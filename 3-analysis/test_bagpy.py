# # VPN, no img
# "2021-05-26-17-49-23.bag"
# 0  /joint_group_position_controller/command  std_msgs/Float64MultiArray         428601  6034.969784
# 1                             /joint_states      sensor_msgs/JointState           8959   137.290846
# 2                 /relaxed_ik/ee_pose_goals      relaxed_ik/EEPoseGoals           7167   100.007248
# 3                                       /tf          tf2_msgs/TFMessage         442102  6150.005865
# 4                                /timestamp            std_msgs/Float64             72     1.000002
# 5                           /unity/gripper1            std_msgs/Float32           3104   120.741090
# 6                             /unity/twist1         geometry_msgs/Twist           3103   120.624765
# # VPN, two cameras
# "2021-05-26-17-53-00.bag"
# 0  /joint_group_position_controller/command  std_msgs/Float64MultiArray         199057  5866.159441
# 1                             /joint_states      sensor_msgs/JointState           4297   137.304329
# 2                 /relaxed_ik/ee_pose_goals      relaxed_ik/EEPoseGoals           3438   100.009633
# 3                                       /tf          tf2_msgs/TFMessage         205674  5991.862857
# 4                                /timestamp            std_msgs/Float64             35     0.999998
# 5                           /unity/gripper1            std_msgs/Float32           3750   123.369139
# 6                             /unity/twist1         geometry_msgs/Twist           3749   123.068689
# # Ethernet, no img
# "2021-05-26-18-07-03.bag"
# 0  /joint_group_position_controller/command  std_msgs/Float64MultiArray         211323  6009.031519
# 1                             /joint_states      sensor_msgs/JointState           4443   137.356039
# 2                 /relaxed_ik/ee_pose_goals      relaxed_ik/EEPoseGoals           3555   100.002480
# 3                                       /tf          tf2_msgs/TFMessage         218042  6132.023392
# 4                           /unity/gripper1            std_msgs/Float32           3878   124.404686
# 5                             /unity/twist1         geometry_msgs/Twist           3878   119.868080
# # Ethernet, two cameras
# "2021-05-26-18-08-27.bag"
# 0  /joint_group_position_controller/command  std_msgs/Float64MultiArray         291433  5882.614306
# 1                             /joint_states      sensor_msgs/JointState           6274   137.369535
# 2                 /relaxed_ik/ee_pose_goals      relaxed_ik/EEPoseGoals           5020   100.000095
# 3                                       /tf          tf2_msgs/TFMessage         301062  6009.031519
# 4                           /unity/gripper1            std_msgs/Float32           5467   122.443556
# 5                             /unity/twist1         geometry_msgs/Twist           5467   115.999336

import bagpy
from bagpy import bagreader
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# folder_path = Path(__file__).resolve().parents[1] / 'test_logs/bags'
folder_path = Path(
    "E:\\forROS\\RAIN2\\teleoperation_logs\\").resolve() / 'test_logs'


def create_csv_from_bag(file_path):

    b = bagreader(file_path)
    print(b.topic_table)
    data1 = b.message_by_topic('/unity/twist1')
    data2 = b.message_by_topic('/unity/gripper1')
    data3 = b.message_by_topic('/clock')
    data4 = b.message_by_topic('/timestamp')
    data5 = b.message_by_topic('/tf')
    data6 = b.message_by_topic('/relaxed_ik/ee_pose_goals')
    data7 = b.message_by_topic('/joint_states')
    data8 = b.message_by_topic('/joint_group_position_controller/command')

    return{data1, data2, data3, data4, data5, data6, data7, data8}


bag_files_ = {
    "2021-05-26-17-49-23.bag",
    "2021-05-26-17-53-00.bag",
    "2021-05-26-18-07-03.bag",
    "2021-05-26-18-08-27.bag"}

# Example, create CSV files
# for index, file in enumerate(bag_files_):
#     print(file)
#     file_path = str(folder_path / file)
#     print(file_path)
#     # csvs.add(create_csv_from_bag(file_path))
#     create_csv_from_bag(file_path)

folders_paths = {
    "2021-05-26-17-49-23",
    "2021-05-26-17-53-00",
    "2021-05-26-18-07-03",
    "2021-05-26-18-08-27"}

fyle_types = {
    "relaxed_ik-ee_pose_goals.csv",
    "joint_group_position_controller-command.csv",
    "joint_states.csv",
    "unity-twist1.csv"}

print("Test bags import csvs")
pd_diffs = pd.DataFrame(columns=['diff'])
pd_ddesc = pd.DataFrame(columns=['describe'])
pd_speed = pd.DataFrame(columns=['A'])
pd_effort = pd.DataFrame(columns=['A'])

for indexa, folder in enumerate(folders_paths):
    temp_diffs = pd.DataFrame(columns=['diff'])
    temp_ddesc = pd.DataFrame(columns=['describe'])

    for indexb, file in enumerate(fyle_types):
        file_path = str(folder_path / "bags" / folder / file)

        data = pd.read_csv(file_path)
        temp_diffs.loc[indexb] = [(data['Time']*1000).diff()]
        temp_ddesc.loc[indexb] = [(data['Time']*1000).diff().describe()]
        if file == "joint_states.csv":
            pd_speed.loc[indexa] = [
                data[['velocity_0', 'velocity_1', 'velocity_2', 'velocity_3', 'velocity_4', 'velocity_5']]]
            pd_effort.loc[indexa] = [
                data[['effort_0', 'effort_1', 'effort_2', 'effort_3', 'effort_4', 'effort_5']]]

        plt.plot(data['Time'].diff())
        print(temp_ddesc.loc[indexb])

    pd_diffs.loc[indexa] = [temp_diffs]
    pd_ddesc.loc[indexa] = [temp_ddesc]

print("joint_states speeds")
print(pd_speed.loc[0]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[1]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[2]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[3]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print("joint_states efforts")
print(pd_effort.loc[0]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_effort.loc[1]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_effort.loc[2]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_effort.loc[3]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print("joint_states speeds diff")
print(pd_speed.loc[0]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[1]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[2]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[3]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])


print("joint_states")
print(pd_ddesc.loc[2]['describe'].loc[0].tolist())
print(pd_ddesc.loc[2]['describe'].loc[1].tolist())
print(pd_ddesc.loc[2]['describe'].loc[2].tolist())
print(pd_ddesc.loc[2]['describe'].loc[3].tolist())
print("unity-twist1")
print(pd_ddesc.loc[3]['describe'].loc[0].tolist())
print(pd_ddesc.loc[3]['describe'].loc[1].tolist())
print(pd_ddesc.loc[3]['describe'].loc[2].tolist())
print(pd_ddesc.loc[3]['describe'].loc[3].tolist())
print("joint_group_position_controller")
print(pd_ddesc.loc[1]['describe'].loc[0].tolist())
print(pd_ddesc.loc[1]['describe'].loc[1].tolist())
print(pd_ddesc.loc[1]['describe'].loc[2].tolist())
print(pd_ddesc.loc[1]['describe'].loc[3].tolist())


unity_csv = [{"1622044383423_Joints.csv",
              "1622044383656_Twist.csv",
              "1622044383670_Gripper.csv"},

             {"1622044626621_Joints.csv",
              "1622044626633_Img.csv",
              "1622044626634_Img.csv",
              "1622044626817_Twist.csv",
              "1622044626828_Gripper.csv"},

             {"1622045545570_Joints.csv",
              "1622045545806_Twist.csv",
              "1622045545809_Gripper.csv"},

             {"1622045638867_Joints.csv",
              "1622045638894_Img.csv",
              "1622045638895_Img.csv",
              "1622045639090_Twist.csv",
              "1622045639101_Gripper.csv"}]


print("Test unity import csvs")
pdu_diffs = pd.DataFrame(columns=['diff'])
pdu_ddesc = pd.DataFrame(columns=['describe'])

for indexa, experiment in enumerate(unity_csv):
    tempu_diffs = pd.DataFrame(columns=['diff'])
    tempu_ddesc = pd.DataFrame(columns=['describe'])

    for indexb, file in enumerate(experiment):
        file_path = str(folder_path / "csv" / file)

        data = pd.read_csv(file_path, index_col=False)
        tempu_diffs.loc[indexb] = [(data['time']*1000).diff()]
        tempu_ddesc.loc[indexb] = [(data['time']*1000).diff().describe()]
        plt.plot(data['time'].diff())
        print(tempu_ddesc.loc[indexb])

    pdu_diffs.loc[indexa] = [tempu_diffs]
    pdu_ddesc.loc[indexa] = [tempu_ddesc]


unity_joints_csv = ["1622044383423_Joints.csv",
                    "1622044626621_Joints.csv",
                    "1622045545570_Joints.csv",
                    "1622045638867_Joints.csv"]

pdjoints_ = pd.DataFrame(columns=['A'])
pdjoints_ddesc = pd.DataFrame(columns=['A'])

for indexa, file in enumerate(unity_joints_csv):

    file_path = str(folder_path / "csv" / file)

    data = pd.read_csv(file_path, index_col=False)
    pdjoints_.loc[indexa] = [data[['time', 'elbow_joint', 'shoulder_lift_joint',
                                   'shoulder_pan_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']]]
    pdjoints_ddesc.loc[indexa] = [data[['time', 'elbow_joint', 'shoulder_lift_joint',
                                        'shoulder_pan_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().abs().describe()]

print("joint_states desc ime diff")
print(pdjoints_ddesc.loc[0]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[1]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[2]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[3]['A']['time'].loc[['mean', 'std', 'max', 'min']])

print("joint_states desc positions diff")
print(pdjoints_ddesc.loc[0]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[1]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[2]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[3]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])

print("joint_states vel positions diff")
print((pdjoints_.loc[0]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[0]['A']['time'].diff().dropna(), axis=0)).describe())
print((pdjoints_.loc[1]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[1]['A']['time'].diff().dropna(), axis=0)).describe())
print((pdjoints_.loc[2]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[2]['A']['time'].diff().dropna(), axis=0)).describe())
print((pdjoints_.loc[3]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[3]['A']['time'].diff().dropna(), axis=0)).describe())


unity_twist_csv = ["1622044383656_Twist.csv",
                   "1622044626817_Twist.csv",
                   "1622045545806_Twist.csv",
                   "1622045639090_Twist.csv"]

pdtwist_ = pd.DataFrame(columns=['A'])
pdtwist_ddesc = pd.DataFrame(columns=['A'])

for indexa, file in enumerate(unity_twist_csv):

    file_path = str(folder_path / "csv" / file)

    data = pd.read_csv(file_path, index_col=False)
    pdtwist_.loc[indexa] = [
        data[['time', 'l.x', 'l.y', 'l.z', 'a.x', 'a.y', 'a.z']]]
    pdtwist_ddesc.loc[indexa] = [
        data[['time', 'l.x', 'l.y', 'l.z', 'a.x', 'a.y', 'a.z']].diff().abs().describe()]

print("unity-twist1")
print(pdtwist_ddesc.loc[0]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdtwist_ddesc.loc[1]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdtwist_ddesc.loc[2]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdtwist_ddesc.loc[3]['A']['time'].loc[['mean', 'std', 'max', 'min']])

unity_img_csv = ["1622044626633_Img.csv",
                 "1622044626634_Img.csv",
                 "1622045638894_Img.csv",
                 "1622045638895_Img.csv"]

pdimg_ = pd.DataFrame(columns=['A'])
pdimg_ddesc = pd.DataFrame(columns=['A'])

for indexa, file in enumerate(unity_img_csv):

    file_path = str(folder_path / "csv" / file)

    data = pd.read_csv(file_path, index_col=False)
    pdimg_.loc[indexa] = [
        data[['time']]]
    pdimg_ddesc.loc[indexa] = [
        data[['time']].diff().abs().describe()]

print("img")
print(pdimg_ddesc.loc[0]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdimg_ddesc.loc[1]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdimg_ddesc.loc[2]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdimg_ddesc.loc[3]['A']['time'].loc[['mean', 'std', 'max', 'min']])
