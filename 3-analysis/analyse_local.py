"""
Analyse data from the local side.
"""
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

folder_path = Path(__file__).resolve().parents[1] / 'data/local'

unity_csv = [{"1622044383423_Joints.csv",
              "1622044383656_Twist.csv",
              "1622044383670_Gripper.csv"},

             {"1622044626621_Joints.csv",
              "1622044626633_Img.csv",
              "1622044626634_Img.csv",
              "1622044626817_Twist.csv",
              "1622044626828_Gripper.csv"},

             {"1622045545570_Joints.csv",
              "1622045545806_Twist.csv",
              "1622045545809_Gripper.csv"},

             {"1622045638867_Joints.csv",
              "1622045638894_Img.csv",
              "1622045638895_Img.csv",
              "1622045639090_Twist.csv",
              "1622045639101_Gripper.csv"}]


print("Test unity import csvs")
pdu_diffs = pd.DataFrame(columns=['diff'])
pdu_ddesc = pd.DataFrame(columns=['describe'])

for indexa, experiment in enumerate(unity_csv):
    tempu_diffs = pd.DataFrame(columns=['diff'])
    tempu_ddesc = pd.DataFrame(columns=['describe'])

    for indexb, file in enumerate(experiment):
        file_path = str(folder_path / "csv" / file)

        data = pd.read_csv(file_path, index_col=False)
        tempu_diffs.loc[indexb] = [(data['time']*1000).diff()]
        tempu_ddesc.loc[indexb] = [(data['time']*1000).diff().describe()]
        plt.plot(data['time'].diff())
        print(tempu_ddesc.loc[indexb])

    pdu_diffs.loc[indexa] = [tempu_diffs]
    pdu_ddesc.loc[indexa] = [tempu_ddesc]


unity_joints_csv = ["1622044383423_Joints.csv",
                    "1622044626621_Joints.csv",
                    "1622045545570_Joints.csv",
                    "1622045638867_Joints.csv"]

pdjoints_ = pd.DataFrame(columns=['A'])
pdjoints_ddesc = pd.DataFrame(columns=['A'])

for indexa, file in enumerate(unity_joints_csv):

    file_path = str(folder_path / "csv" / file)

    data = pd.read_csv(file_path, index_col=False)
    pdjoints_.loc[indexa] = [data[['time', 'elbow_joint', 'shoulder_lift_joint',
                                   'shoulder_pan_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']]]
    pdjoints_ddesc.loc[indexa] = [data[['time', 'elbow_joint', 'shoulder_lift_joint',
                                        'shoulder_pan_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().abs().describe()]

print("joint_states desc ime diff")
print(pdjoints_ddesc.loc[0]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[1]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[2]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[3]['A']['time'].loc[['mean', 'std', 'max', 'min']])

print("joint_states desc positions diff")
print(pdjoints_ddesc.loc[0]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[1]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[2]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])
print(pdjoints_ddesc.loc[3]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].loc[['mean', 'std', 'max', 'min']])

print("joint_states vel positions diff")
print((pdjoints_.loc[0]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[0]['A']['time'].diff().dropna(), axis=0)).describe())
print((pdjoints_.loc[1]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[1]['A']['time'].diff().dropna(), axis=0)).describe())
print((pdjoints_.loc[2]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[2]['A']['time'].diff().dropna(), axis=0)).describe())
print((pdjoints_.loc[3]['A'][['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint',
      'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']].diff().dropna().div(pdjoints_.loc[3]['A']['time'].diff().dropna(), axis=0)).describe())


unity_twist_csv = ["1622044383656_Twist.csv",
                   "1622044626817_Twist.csv",
                   "1622045545806_Twist.csv",
                   "1622045639090_Twist.csv"]

pdtwist_ = pd.DataFrame(columns=['A'])
pdtwist_ddesc = pd.DataFrame(columns=['A'])

for indexa, file in enumerate(unity_twist_csv):

    file_path = str(folder_path / "csv" / file)

    data = pd.read_csv(file_path, index_col=False)
    pdtwist_.loc[indexa] = [
        data[['time', 'l.x', 'l.y', 'l.z', 'a.x', 'a.y', 'a.z']]]
    pdtwist_ddesc.loc[indexa] = [
        data[['time', 'l.x', 'l.y', 'l.z', 'a.x', 'a.y', 'a.z']].diff().abs().describe()]

print("unity-twist1")
print(pdtwist_ddesc.loc[0]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdtwist_ddesc.loc[1]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdtwist_ddesc.loc[2]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdtwist_ddesc.loc[3]['A']['time'].loc[['mean', 'std', 'max', 'min']])

unity_img_csv = ["1622044626633_Img.csv",
                 "1622044626634_Img.csv",
                 "1622045638894_Img.csv",
                 "1622045638895_Img.csv"]

pdimg_ = pd.DataFrame(columns=['A'])
pdimg_ddesc = pd.DataFrame(columns=['A'])

for indexa, file in enumerate(unity_img_csv):

    file_path = str(folder_path / "csv" / file)

    data = pd.read_csv(file_path, index_col=False)
    pdimg_.loc[indexa] = [
        data[['time']]]
    pdimg_ddesc.loc[indexa] = [
        data[['time']].diff().abs().describe()]

print("img")
print(pdimg_ddesc.loc[0]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdimg_ddesc.loc[1]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdimg_ddesc.loc[2]['A']['time'].loc[['mean', 'std', 'max', 'min']])
print(pdimg_ddesc.loc[3]['A']['time'].loc[['mean', 'std', 'max', 'min']])
