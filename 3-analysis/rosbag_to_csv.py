"""
Script to create a csv from the available rosbags.
"""
from pathlib import Path
import bagpy
from bagpy import bagreader

folder_path = Path(__file__).resolve().parents[1] / 'data/remote'


def create_csv_from_bag(file_path):

    b = bagreader(file_path)
    print(b.topic_table)
    data1 = b.message_by_topic('/unity/twist1')
    data2 = b.message_by_topic('/unity/gripper1')
    data3 = b.message_by_topic('/clock')
    data4 = b.message_by_topic('/timestamp')
    data5 = b.message_by_topic('/tf')
    data6 = b.message_by_topic('/relaxed_ik/ee_pose_goals')
    data7 = b.message_by_topic('/joint_states')
    data8 = b.message_by_topic('/joint_group_position_controller/command')

    return{data1, data2, data3, data4, data5, data6, data7, data8}


bag_files_ = {
    "2021-05-26-17-49-23.bag",
    "2021-05-26-17-53-00.bag",
    "2021-05-26-18-07-03.bag",
    "2021-05-26-18-08-27.bag"}

# Create CSV files
for index, file in enumerate(bag_files_):
    print(file)
    file_path = str(folder_path / file)
    print(file_path)
    create_csv_from_bag(file_path)
