"""
Analyse data from the remote side.
"""
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

folder_path = Path(__file__).resolve().parents[1] / 'data/remote'

folders_paths = {
    "2021-05-26-17-49-23",
    "2021-05-26-17-53-00",
    "2021-05-26-18-07-03",
    "2021-05-26-18-08-27"}

fyle_types = {
    "relaxed_ik-ee_pose_goals.csv",
    "joint_group_position_controller-command.csv",
    "joint_states.csv",
    "unity-twist1.csv"}

print("Test bags import csvs")
pd_diffs = pd.DataFrame(columns=['diff'])
pd_ddesc = pd.DataFrame(columns=['describe'])
pd_speed = pd.DataFrame(columns=['A'])
pd_effort = pd.DataFrame(columns=['A'])

for indexa, folder in enumerate(folders_paths):
    temp_diffs = pd.DataFrame(columns=['diff'])
    temp_ddesc = pd.DataFrame(columns=['describe'])

    for indexb, file in enumerate(fyle_types):
        file_path = str(folder_path / "bags" / folder / file)

        data = pd.read_csv(file_path)
        temp_diffs.loc[indexb] = [(data['Time']*1000).diff()]
        temp_ddesc.loc[indexb] = [(data['Time']*1000).diff().describe()]
        if file == "joint_states.csv":
            pd_speed.loc[indexa] = [
                data[['velocity_0', 'velocity_1', 'velocity_2', 'velocity_3', 'velocity_4', 'velocity_5']]]
            pd_effort.loc[indexa] = [
                data[['effort_0', 'effort_1', 'effort_2', 'effort_3', 'effort_4', 'effort_5']]]

        plt.plot(data['Time'].diff())
        print(temp_ddesc.loc[indexb])

    pd_diffs.loc[indexa] = [temp_diffs]
    pd_ddesc.loc[indexa] = [temp_ddesc]

print("joint_states speeds")
print(pd_speed.loc[0]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[1]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[2]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[3]['A'].describe().loc[['mean', 'std', 'max', 'min']])
print("joint_states efforts")
print(pd_effort.loc[0]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_effort.loc[1]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_effort.loc[2]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_effort.loc[3]['A'].abs().describe(
).loc[['mean', 'std', 'max', 'min']])
print("joint_states speeds diff")
print(pd_speed.loc[0]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[1]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[2]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])
print(pd_speed.loc[3]['A'].diff().describe(
).loc[['mean', 'std', 'max', 'min']])


print("joint_states")
print(pd_ddesc.loc[2]['describe'].loc[0].tolist())
print(pd_ddesc.loc[2]['describe'].loc[1].tolist())
print(pd_ddesc.loc[2]['describe'].loc[2].tolist())
print(pd_ddesc.loc[2]['describe'].loc[3].tolist())
print("unity-twist1")
print(pd_ddesc.loc[3]['describe'].loc[0].tolist())
print(pd_ddesc.loc[3]['describe'].loc[1].tolist())
print(pd_ddesc.loc[3]['describe'].loc[2].tolist())
print(pd_ddesc.loc[3]['describe'].loc[3].tolist())
print("joint_group_position_controller")
print(pd_ddesc.loc[1]['describe'].loc[0].tolist())
print(pd_ddesc.loc[1]['describe'].loc[1].tolist())
print(pd_ddesc.loc[1]['describe'].loc[2].tolist())
print(pd_ddesc.loc[1]['describe'].loc[3].tolist())
